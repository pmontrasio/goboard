require "intersection"

class Board
  # This is a 4x4 board
  # . O X .
  # . O X X
  # O O X .
  # . O X .
  def initialize(size, text_board="")
    @size = size
    validate_board(text_board)
    create_board
    add_corners
    add_sides
    add_center
    return if text_board == ""
    load_board(text_board)
  end

  def size
    @size
  end

  # Base 1
  def at(row, column)
    @board[row - 1][column - 1]
  end

  # Base 1
  def black!(row, column)
    intersection = @board[row - 1][column - 1]
    raise Exception.new("Intersection #{row}, #{column} not empty") unless intersection.empty?
    intersection.black!
    intersection.neighbors.each do |neighbor|
      next unless neighbor.white?
      next unless neighbor.liberties == 0
      opponent_group = group(neighbor)
      if opponent_group.map {|stone| stone.liberties}.uniq == [0]
        opponent_group.each do |stone|
          stone.empty!
        end
      end
    end
    maybe_remove_group(row, column)
  end

  # Base 1
  def white!(row, column)
    intersection = @board[row - 1][column - 1]
    raise Exception.new("Intersection #{row}, #{column} not empty") unless intersection.empty?
    intersection.white!
    intersection.neighbors.each do |neighbor|
      next unless neighbor.black?
      next unless neighbor.liberties == 0
      opponent_group = group(neighbor)
      if opponent_group.map {|stone| stone.liberties}.uniq == [0]
        opponent_group.each do |stone|
          stone.empty!
        end
      end
    end
    maybe_remove_group(row, column)
  end

  def to_javascript
    ""
  end

  def show_liberties
    @board.map do |row|
      row.map{|intersection| intersection.liberties.to_s}.join(" ")
    end.join("\n")
  end

  def show_neighbors
    @board.map do |row|
      row.map{|intersection| intersection.neighbors.size.to_s}.join(" ")
    end.join("\n")
  end

  def to_text
    color_to_text = {empty: ".", white: "O", black: "X"}
    @board.map { |row|
      row.map{|intersection| color_to_text[intersection.color]}.join(" ")
      #row.map{|intersection| intersection.color}.join(" ")
    }.join("\n")
  end

  def group(intersection)
    return [] if intersection.empty?
    add_neighbors_with_color(intersection, intersection.color, [intersection])
  end

  private

  # Base 1
  def maybe_remove_group(row, column)
    return if @board[row - 1][column - 1].empty?
    return unless @board[row - 1][column - 1].liberties == 0
    intersections = group(@board[row - 1][column - 1])
    intersections.each do |intersection|
      return if intersection.liberties > 0
    end
    intersections.each do |intersection|
      intersection.empty!
    end
  end

  def add_neighbors_with_color(intersection, color, this_group)
    intersections = intersection.neighbors.select {|intersection| intersection.color == color}
    intersections.each do |intersection|
      next if this_group.include?(intersection)
      this_group << intersection
      add_neighbors_with_color(intersection, color, this_group)
    end
    this_group
  end

  def validate_board(text_board)
    return if text_board
    lines = text_board.split("\n")
    if @size != lines.length * 2 - 1 # N intersections and N - 1 spaces between them
      raise Exception.new("There must be #{@size} lines but there only #{lines.length} of them ")
    end
    line_lenghts = lines.map {|line| line.length}.uniq
    if line_lenghts.size != 1 || line_lengths[0] != @size * 2 - 1
      raise Exception.new("All the lines in the board must contain #{@size} intersections separated by spaces")
    end
  end

  def create_board
    @board = Array.new(@size) do |row|
      Array.new(@size) do |intersection|
        Intersection.new
      end
    end
  end

  def add_corners
    @board[0][0].set_neighbors([@board[0][1], @board[1][0]])
    @board[0][@size - 1].set_neighbors([@board[0][@size - 2], @board[1][@size - 1]])
    @board[@size - 1][0].set_neighbors([@board[@size - 2][0], @board[@size - 1][1]])
    @board[@size - 1][@size - 1].set_neighbors([@board[@size - 2][@size - 1], @board[@size - 1][@size - 2]])
  end

  def add_sides
    (1..@size - 2).each do |n|
      @board[0][n].set_neighbors([@board[0][n - 1], @board[1][n], @board[0][n + 1]])
      @board[@size - 1][n].set_neighbors([@board[@size - 1][n - 1], @board[@size - 2][n], @board[@size - 1][n + 1]])
      @board[n][0].set_neighbors([@board[n - 1][0], @board[n][1], @board[n + 1][0]])
      @board[n][@size - 1].set_neighbors([@board[n - 1][@size - 1], @board[n][@size - 2], @board[n + 1][@size - 1]])
    end
  end

  def add_center
    (1..@size - 2).each do |row|
      (1..@size - 2).each do |column|
        @board[row][column].set_neighbors([@board[row - 1][column], @board[row + 1][column], @board[row][column - 1], @board[row][column + 1]])
      end
    end
  end

  def load_board(valid_text_board)
    row = 1
    column = 1
    valid_text_board.split("\n").each do |line|
      line.each_char do |color|
        case color
        when "."
          column += 1
        when "X"
          black!(row, column)
          column += 1
        when "O"
          white!(row, column)
          column += 1
        end
      end
      row += 1
      column = 1
    end
  end
end
