class Intersection
  def initialize()
    @status = :empty
    @liberties = 0 # there are no neighbors
    @neighbors = []
  end

  def neighbors
    @neighbors
  end

  def add_neighbor(intersection)
    return if intersection == self
    return if @neighbors.include?(intersection)
    @neighbors << intersection
    @liberties += 1 if intersection.empty?
    intersection.add_neighbor(self)
  end

  def remove_neighbor(intersection)
    return if intersection == self
    return unless @neighbors.include?(intersection)
    @neighbors.delete(intersection)
    @liberties -= 1 if intersection.empty?
    intersection.remove_neighbor(self)
  end

  def set_neighbors(neighbors)
    @neighbors.each do |neighbor|
      self.remove_neighbor(neighbor)
    end
    neighbors.each do |neighbor|
      self.add_neighbor(neighbor)
    end
  end

  def liberties
    @liberties
  end

  def color
    @status
  end

  def empty?
    @status == :empty
  end

  def black?
    @status == :black
  end

  def white?
    @status == :white
  end

  def black!
    raise Exception.new("Intersection not empty: it is #{@status}") unless self.empty?
    @status = :black
    @neighbors.each {|intersection| intersection.new_black_neighbor!}
  end

  def white!
    raise Exception.new("Intersection not empty: it is #{@status}") unless self.empty?
    @status = :white
    @neighbors.each {|intersection| intersection.new_white_neighbor!}
  end

  def empty!
    @status = :empty
    @neighbors.each {|intersection| intersection.new_empty_neighbor!}
  end

  def new_black_neighbor!
    @liberties -= 1
  end

  def new_white_neighbor!
    @liberties -= 1
  end

  def new_empty_neighbor!
    @liberties += 1
  end

end
