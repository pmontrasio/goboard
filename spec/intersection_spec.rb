require "intersection"

RSpec.describe Intersection do
  it "starts empty" do
    intersection = Intersection.new
    expect(intersection.empty?).to be true
    expect(intersection.liberties).to eq(0)
    expect(intersection.neighbors.size).to eq(0)
  end

  it "can have a neighbor" do
    intersection_1 = Intersection.new
    intersection_2 = Intersection.new
    intersection_1.add_neighbor(intersection_2)
    expect(intersection_1.liberties).to eq(1)
    expect(intersection_2.liberties).to eq(1)
    expect(intersection_1.neighbors).to eq([intersection_2])
    expect(intersection_2.neighbors).to eq([intersection_1])
  end

  it "can lose a neighbor" do
    intersection_1 = Intersection.new
    intersection_2 = Intersection.new
    intersection_1.add_neighbor(intersection_2)
    intersection_1.remove_neighbor(intersection_2)
    expect(intersection_1.liberties).to eq(0)
    expect(intersection_2.liberties).to eq(0)
    expect(intersection_1.neighbors).to eq([])
    expect(intersection_2.neighbors).to eq([])
  end

  it "starts with as many liberties as neighbors" do
    [2, 3, 4].each do |number_of_neighbors|
      intersection = Intersection.new
      neighbors = Array.new(number_of_neighbors) { Intersection.new }
      intersection.set_neighbors(neighbors)
      expect(intersection.liberties).to eq(number_of_neighbors)
    end
  end

  it "can become black" do
    intersection = Intersection.new
    number_of_neighbors = 4
    neighbors = Array.new(number_of_neighbors) { Intersection.new }
    intersection.set_neighbors(neighbors)
    intersection.black!
    expect(intersection.empty?).to be false
    expect(intersection.black?).to be true
    expect(intersection.white?).to be false
    expect(intersection.liberties).to eq(number_of_neighbors)
    neighbors.each do |neighbor|
      expect(neighbor.liberties).to eq(0)
    end
  end

  it "can become white" do
    intersection = Intersection.new
    number_of_neighbors = 4
    neighbors = Array.new(number_of_neighbors) { Intersection.new }
    intersection.set_neighbors(neighbors)
    intersection.white!
    expect(intersection.empty?).to be false
    expect(intersection.black?).to be false
    expect(intersection.white?).to be true
    expect(intersection.liberties).to eq(number_of_neighbors)
  end

  it "loses and gains liberties when stones are played and removed nearby" do
    intersection = Intersection.new
    number_of_neighbors = 4
    neighbors = Array.new(number_of_neighbors) { Intersection.new }
    intersection.set_neighbors(neighbors)

    neighbors[0].white!
    expect(intersection.liberties).to eq(number_of_neighbors - 1)
    neighbors[1].black!
    expect(intersection.liberties).to eq(number_of_neighbors - 2)
    neighbors[2].white!
    expect(intersection.liberties).to eq(number_of_neighbors - 3)
    neighbors[3].black!
    expect(intersection.liberties).to eq(number_of_neighbors - 4)

    neighbors[3].empty!
    expect(intersection.liberties).to eq(number_of_neighbors - 3)
    neighbors[2].empty!
    expect(intersection.liberties).to eq(number_of_neighbors - 2)
    neighbors[1].empty!
    expect(intersection.liberties).to eq(number_of_neighbors - 1)
    neighbors[0].empty!
    expect(intersection.liberties).to eq(number_of_neighbors)
  end
end
