require "board"

RSpec.describe Board do
  describe "can be created empty" do
    it "works for a 4x4 board" do
      board = Board.new(4)
      expect(board.size).to eq(4)
      expect(board.to_text).to eq(". . . .\n. . . .\n. . . .\n. . . .")
      expect(board.show_liberties).to eq("2 3 3 2\n3 4 4 3\n3 4 4 3\n2 3 3 2")
      expect(board.show_neighbors).to eq("2 3 3 2\n3 4 4 3\n3 4 4 3\n2 3 3 2")
    end

    it "works for a 5x5 board" do
      board = Board.new(5)
      expect(board.size).to eq(5)
      expect(board.to_text).to eq(". . . . .\n. . . . .\n. . . . .\n. . . . .\n. . . . .")
      expect(board.show_liberties).to eq("2 3 3 3 2\n3 4 4 4 3\n3 4 4 4 3\n3 4 4 4 3\n2 3 3 3 2")
      expect(board.show_neighbors).to eq("2 3 3 3 2\n3 4 4 4 3\n3 4 4 4 3\n3 4 4 4 3\n2 3 3 3 2")
    end
  end

  it "can be created from a text representation" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    expect(board.size).to eq(4)
    expect(board.to_text).to eq(text_board)
  end

  it "can't add a stone on a not empty intersection" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    expect{board.black!(1, 2)}.to raise_error(Exception, "Intersection 1, 2 not empty")
    expect{board.white!(1, 2)}.to raise_error(Exception, "Intersection 1, 2 not empty")
  end

  it "can add a black stone" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    expect(board.at(1, 4).empty?).to be true
    board.black!(1, 4)
    expect(board.at(1, 4).black?).to be true
  end

  it "can add a white stone" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    expect(board.at(1, 1).empty?).to be true
    board.white!(1, 1)
    expect(board.at(1, 1).white?).to be true
  end

  it "yields a black group starting with a black stone" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    [[1, 3], [2, 3], [2, 4], [3, 3], [4, 3]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      stone = board.at(row, column)
      group = board.group(stone)
      expect(group.size).to eq(text_board.count("X"))
      expect(group.include?(board.at(1,1))).to be false
      expect(group.include?(board.at(1,2))).to be false
      expect(group.include?(board.at(1,3))).to be true
      expect(group.include?(board.at(1,4))).to be false
      expect(group.include?(board.at(2,1))).to be false
      expect(group.include?(board.at(2,2))).to be false
      expect(group.include?(board.at(2,3))).to be true
      expect(group.include?(board.at(2,4))).to be true
      expect(group.include?(board.at(3,1))).to be false
      expect(group.include?(board.at(3,2))).to be false
      expect(group.include?(board.at(3,3))).to be true
      expect(group.include?(board.at(3,4))).to be false
      expect(group.include?(board.at(4,1))).to be false
      expect(group.include?(board.at(4,2))).to be false
      expect(group.include?(board.at(4,3))).to be true
      expect(group.include?(board.at(4,4))).to be false
    end
  end

  it "yields a white group starting with a white stone" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    [[1, 2], [2, 2], [3, 1], [3, 2], [4, 2]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      stone = board.at(row, column)
      group = board.group(stone)
      expect(group.size).to eq(text_board.count("X"))
      expect(group.include?(board.at(1,1))).to be false
      expect(group.include?(board.at(1,2))).to be true
      expect(group.include?(board.at(1,3))).to be false
      expect(group.include?(board.at(1,4))).to be false
      expect(group.include?(board.at(2,1))).to be false
      expect(group.include?(board.at(2,2))).to be true
      expect(group.include?(board.at(2,3))).to be false
      expect(group.include?(board.at(2,4))).to be false
      expect(group.include?(board.at(3,1))).to be true
      expect(group.include?(board.at(3,2))).to be true
      expect(group.include?(board.at(3,3))).to be false
      expect(group.include?(board.at(3,4))).to be false
      expect(group.include?(board.at(4,1))).to be false
      expect(group.include?(board.at(4,2))).to be true
      expect(group.include?(board.at(4,3))).to be false
      expect(group.include?(board.at(4,4))).to be false
    end
  end

  it "yields an empty group starting with an empty intersection" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    [[1, 1], [1, 4], [2, 1], [3, 4], [4, 1], [4, 4]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      stone = board.at(row, column)
      group = board.group(stone)
      expect(group.size).to eq(0)
    end
  end

  it "yields a single stone black group starting with single black stones" do
    text_board = "X . X . X\n. X . X .\nX . X . X\n. X . X .\nX . X . X"
    board = Board.new(5, text_board)
    [[1, 1], [1, 5], [5, 1], [5, 5], # corners
     [1, 3], [3, 1], [3, 5], [5, 3], # sides
     [2, 2], [2, 4], [4, 2], [4, 4] # center
    ].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      stone = board.at(row, column)
      group = board.group(stone)
      expect(group.size).to eq(1)
      expect(group).to eq([stone])
    end
  end

  it "removes a captured black group" do
    text_board = ". O X X\n. O X X\nO O X X\n. O X ."
    board = Board.new(4, text_board)
    board.white!(4, 4)
    # empty stay empty
    [[1, 1], [2, 1], [4, 1]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).empty?).to be true
    end
    # black becomes empty
    [[1, 3], [1, 4], [2, 3], [2, 4], [3, 3], [3, 4], [4, 3]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).empty?).to be true
    end
    # white stays white
    [[1, 2], [2, 2], [3, 1], [3, 2], [4, 2], [4, 4]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).white?).to be true
    end
  end

  it "removes a captured white group" do
    text_board = "O O X .\nO O X .\nO O X X\n. O X ."
    board = Board.new(4, text_board)
    board.black!(4, 1)
    # empty stay empty
    [[1, 4], [2, 4], [4, 4]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).empty?).to be true
    end
    # black stays black
    [[1, 3], [2, 3], [3, 3], [3, 4], [4, 1], [4, 3]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).black?).to be true
    end
    # white becomes empty
    [[1, 2], [2, 2], [3, 1], [3, 2], [4, 2]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).empty?).to be true
    end
  end

  it "removes multiple captured black groups" do
    text_board = ". O X O .\nO O X O O\nX X . X X\nO O X O O\n. O X O ."
    board = Board.new(5, text_board)
    board.white!(3, 3)
    # empty stay empty
    [[1, 1], [5, 1], [5, 1], [5, 5]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).empty?).to be true
    end
    # all blacks are captured
    [[1, 3], [2, 3], [3, 1], [3, 2], [3, 4], [3, 5], [4, 3], [5, 3]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).empty?).to be true
    end
    # all whites stay whites
    [[1, 2], [1, 4], [2, 1], [2, 2], [2, 4], [2, 5], [3, 3], [4, 1], [4, 2], [4, 4], [4, 5], [5, 2], [5, 4]].each do |coordinates|
      row = coordinates[0]
      column = coordinates[1]
      expect(board.at(row, column).white?).to be true
    end
  end

end
