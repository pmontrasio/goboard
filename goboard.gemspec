$LOAD_PATH.push File.expand_path('lib', __dir__)

Gem::Specification.new do |spec|
  spec.name = "GoBoard"
  spec.version = "0.0.2"
  spec.licenses = ["LGPL-3.0-only"]
  spec.authors = ["Paolo Montrasio"]
  spec.email = "paolo@paolomontrasio.com"
  spec.date = "2019-09-22"
  spec.summary = "Board for the game of Go/Baduk/Weiqi"
  spec.description = "Simple board with stone placement and removal of captured groups"
  spec.extra_rdoc_files = ["LICENSE", "README.md"]
  spec.files = ["lib/goboard.rb", "lib/board.rb", "lib/intersection.rb"]
  spec.test_files = `git ls-files -- {test,spec,features}/*`.split("\n")
  spec.require_paths = ["lib"]
  spec.add_development_dependency "rspec", "~> 3.8", ">= 3.8.0"
end
