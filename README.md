# GoBoard

```ruby
board = Board.new(19)
board.black!(4, 4)
board.white!(16, 16)
board.to_text
board.show_liberties

text_board = ". O X X\n. O X X\nO O X X\n. O X ."
board = Board.new(4, text_board)
board.white!(4, 4)
board.to_text # ". O . .\n. O . .\nO O . .\n. O . O"
```

# License

LGPL v3
